<?php 
    require __DIR__ . '/../models/UserModel.php';

    class UserController {
    
        public static function registerUser() {
            $user = new UserModel();
            echo json_encode($user->register(), JSON_UNESCAPED_UNICODE);
        }

        public static function getAllUsers() {
            $users = new UserModel();
            echo json_encode($users->get(), JSON_UNESCAPED_UNICODE);
        }

        public static function deleteUser() {
            $user = new UserModel();
            echo json_encode($user->delete(), JSON_UNESCAPED_UNICODE);
        }

    }
?>