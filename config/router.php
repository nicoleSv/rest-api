<?php 
    class Router {
        private $routes;

        public function __construct() {
            $this->routes = array();
        }

        public function route($method, $request, $callback) {
            $this->routes[] = array('method' => $method,
                                    'request' => $request,
                                    'callback' => $callback);
        }

        public function run() {
            $method = $_SERVER['REQUEST_METHOD'];
            $request = empty($_SERVER['PATH_INFO']) ? '' : explode('/', trim($_SERVER['PATH_INFO'],'/'));

            foreach ($this->routes as $route) {
                if($route['method'] == $method &&
                    $route['request'] == $request[0]) {
                        $route['callback']();
                        return;
                    }
            }
        }
    }
?>