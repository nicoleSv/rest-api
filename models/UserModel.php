<?php
    require 'BaseModel.php';

    class UserModel extends BaseModel {
        // database connection and table name
        protected $tableName = "users";

        // properties
        private $email;
        private $firstName;
        private $lastName;
        private $password;
        private $role;

        public function register() {
            $this->getInput();
            $errors = $this->validate();

            if(empty($errors)) {
                $query = $this->connection->prepare("INSERT INTO users(email, name, lastname, password, role) VALUES (:email, :name, :lastName, :password, :role)");
                $this->password = password_hash($this->password, PASSWORD_DEFAULT);
                $this->role = 'User';
        
                // bind values
                $query->bindParam(":email", $this->email);
                $query->bindParam(":name", $this->firstName);
                $query->bindParam(":lastName", $this->lastName);
                $query->bindParam(":password", $this->password);
                $query->bindParam(":role", $this->role);

                if($query->execute()) {
                    $userObject = array('email' => $this->email,
                                        'name' => $this->firstName,
                                        'lastname' => $this->lastName,
                                        'password' => $this->password,
                                        'role' => $this->role);
                    
                    return $userObject;
                }
                else {
                    http_response_code(400);
                    return array('error' => 'Неуспешно добавяне в базата данни!');
                }
            }
            else {
                http_response_code(400);
                return $errors;
            }
        }

        public function delete() {
            $request = empty($_SERVER['PATH_INFO']) ? '' : explode('/', trim($_SERVER['PATH_INFO'],'/'));
            $this->email = $request[1];

            if($this->isUnique($this->email)) {
                // no such user
                http_response_code(404);
                return array('error' => 'Не съществува потребител с такава електронна поща!');
            }

            $selectQuery = $this->connection->prepare("SELECT * FROM {$this->tableName} WHERE email = :email");
            $selectQuery->bindParam(":email", $this->email);

            $deleteQuery = $this->connection->prepare("DELETE FROM {$this->tableName} WHERE email = :email");
            $deleteQuery->bindParam(":email", $this->email);

            // select user data
            if($selectQuery->execute()) {
                $userData = $selectQuery->fetchAll(PDO::FETCH_ASSOC);

                // then delete user data
                if($deleteQuery->execute()) {
                    return $userData;
                }
                else {
                    http_response_code(400);
                    return array('error' => 'Неуспешно изтриване от базата данни!');
                }
            }
            else {
                http_response_code(400);
                return array('error' => 'Неуспешнa заявка към базата данни!');
            }
        }

        private function getInput() {
            if($_SERVER['CONTENT_TYPE'] === 'application/json') {
                $data = json_decode(file_get_contents('php://input'), true);
                $this->email = $data['email'];
                $this->firstName = $data['name'];
                $this->lastName = $data['lastname'];
                $this->password = $data['password'];
            }
        }

        private function validate() {
           $errors = array();
    
           if(empty($this->email) || empty($this->firstName) ||
            empty($this->lastName) || empty($this->password)) {
                $errors[] = array('error' => 'Не са въведени всички нужни данни!');
            }
            else {
                // validate email
                if(!$this->isValidLength($this->email, 255)) {
                    $errors[] = array('error' => 'Електронната поща трябва да е с максимална дължина 255 символа!');
                }
                elseif(!$this->isValidEmail($this->email)) {
                    $errors[] = array('error' => 'Невалидна електронна поща!');
                }
                elseif(!$this->isUnique($this->email)) {
                    $errors[] = array('error' => 'Съществува потребител с тази електронна поща!');
                }

                // validate names
                if(!$this->isValidLength($this->firstName, 100) ||
                !$this->isValidLength($this->lastName, 100)) {
                    $errors[] = array('error' => 'Името и фамилия са с максимална дължина по 100 символа!');
                }
                elseif(!$this->isCyrillic($this->firstName) || !$this->isCyrillic($this->lastName)) {
                    $errors[] = array('error' => 'Имената трябва да са задължително на кирилица!');
                }
            }

            return $errors;
        }

        private function isValidLength($string, $maxLength) {
            return mb_strlen($string, 'UTF-8') <= $maxLength;
        }

        private function isValidEmail($email) {
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        }

        private function isUnique($email) {
            $query = $this->connection->prepare("SELECT COUNT(email) AS emailsCount FROM {$this->tableName} WHERE email = :email");
            $query->bindParam(":email", $email);

            $result = $query->execute() ? $query->fetch(PDO::FETCH_ASSOC) : 0;
            return $result['emailsCount'] == 0;
        }

        private function isCyrillic($name) {
            return preg_match('/^[\p{Cyrillic}\s\-]+$/u', $name);
        }

        private function isValidRole($role) {
            return $role === 'User' || $role === 'Admin';
        }
    } 
?>