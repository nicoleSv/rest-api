<?php 
    require __DIR__ . '/../config/database.php';

    class BaseModel {
        protected $connection;
        private $db;

        public function __construct() {
            $this->db = new Database();
            $this->connection = $this->db->getConnection();
        }

        public function get() {
            $query = $this->connection->query("SELECT * FROM {$this->tableName}");
            $result = $query !== FALSE ? $query->fetchAll(PDO::FETCH_ASSOC) : '';

            if(empty($result)) {
                http_response_code(404);
                $result = array('error' => 'Неуспешно извличане на данни от базата!');
            }
            return $result;
        }
    }
?>