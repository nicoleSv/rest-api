<?php 
    require 'config/router.php';
    require 'controllers/UserController.php';

    $router = new Router();

    // api.php/register
    $router->route('POST', 'register', function() {
        UserController::registerUser();
    });

    // api.php/users
    $router->route('GET', 'users', function() {
        UserController::getAllUsers();
    });
    
    // api.php/user/:email
    $router->route('DELETE', 'user', function() {
        UserController::deleteUser();
    });

    $router->run();
?>